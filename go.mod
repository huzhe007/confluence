module github.com/anacrolix/confluence

require (
	github.com/anacrolix/envpprof v1.1.0
	github.com/anacrolix/missinggo v1.2.1
	github.com/anacrolix/missinggo/v2 v2.3.2-0.20200110051601-fc3212fb3984
	github.com/anacrolix/tagflag v1.1.0
	github.com/anacrolix/torrent v1.12.0
	github.com/prometheus/client_golang v1.3.0
	github.com/prometheus/common v0.9.1 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
)

go 1.13
